<?php

defined('SYSPATH') or die('No direct script access.');

require_once Kohana::find_file('vendor', 'php-aws/class.s3');

class Kohana_e24files extends S3 {

    private static $_instance = NULL;

    public function __construct($bucket = FALSE) {
        $sBucket = ($bucket) ? $bucket : Kohana::$config->load('e24files.defaultBucket');

        parent::__construct(
                Kohana::$config->load('e24files.account.access_key'), Kohana::$config->load('e24files.account.secret_key'), $sBucket, Kohana::$config->load('e24files.useSSL')
        );
    }

    public static function instance($bucket = FALSE) {
        if (!isset(self::$_instance))
            self::$_instance = new Kohana_e24files($bucket);

        return self::$_instance;
    }

    public function upload($fileName, $remoteName, $public = TRUE) {
        $aFileName = DOCROOT . $fileName;

        if (!file_exists($aFileName) || !is_file($aFileName))
            throw new Kohana_Exception('Could not find file: :file', array(':file' => $aFileName));

        return $this->uploadFile($remoteName, $aFileName, $public);
    }

    public function uploadDir($dirPrefix, $dir, $public = TRUE) {
        $dirPath = DOCROOT . "{$dirPrefix}/{$dir}";

        if (!is_dir($dirPath))
            throw new Kohana_Exception('Could not find dir: :dir', array(':dir' => $dirPath));

        $iterator = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($dirPath), RecursiveIteratorIterator::SELF_FIRST
        );

        foreach ($iterator as $file) {
            $remoteName = str_replace(DOCROOT . $dirPrefix . '/', '', $file);
            $s = $this->uploadFile($remoteName, $file, $public);
            if (!$s) return FALSE;
        }
        return TRUE;
    }

}

