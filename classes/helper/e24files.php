<?php defined('SYSPATH') or die('No direct script access.');

class Helper_e24files {
    public static function asset($url, $bucket = FALSE) {
        $sURL       = ltrim($url, '/');
        $sBucket    = ($bucket) ? trim($bucket) : trim(Kohana::$config->load('e24files.defaultBucket'));
        
        return "http://{$sBucket}.e24files.com/{$sURL}";
    }
}

