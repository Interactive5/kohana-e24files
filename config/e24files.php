<?php defined('SYSPATH') OR die('No direct access allowed.');

return array (
    'account' => array(
        'access_key'    => '',
        'secret_key'    => '',
    ),
    'defaultBucket'     => '',
    'useSSL'            => TRUE,
);